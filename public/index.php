<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/../vendor/autoload.php';

use MyApp\Application;
use MyApp\controllers\ProductContr;
use MyApp\controllers\SiteContr;

$app = new Application();

$app->router->get('/', [SiteContr::class, 'home']);
$app->router->post('/', [ProductContr::class, 'deleteProducts']);
$app->router->get('/add', [SiteContr::class, 'add']);
$app->router->post('/add', [ProductContr::class, 'addNewProduct']);

$app->run();


//sutvarkyti erroru siuntima